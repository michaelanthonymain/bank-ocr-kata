const fs = require('fs');

const testfile = './test-file.txt';
const ZERO =
  ' _ ' +
  '| |' +
  '|_|';
const ONE =
  '   ' +
  '  |' +
  '  |';
const TWO =
  ' _ ' +
  ' _|' +
  '|_ ';
const THREE =
  ' _ ' +
  ' _|' +
  ' _|';
const FOUR =
  '   ' +
  '|_|' +
  '  |';
const FIVE =
  ' _ ' +
  '|_ ' +
  ' _|';
const SIX =
  ' _ ' +
  '|_ ' +
  '|_|';
const SEVEN =
  ' _ ' +
  '  |' +
  '  |';
const EIGHT =
  ' _ ' +
  '|_|' +
  '|_|';
const NINE =
  ' _ ' +
  '|_|' +
  ' _|';

let digitalNumberToNumber = {};
digitalNumberToNumber[ZERO] = 0;
digitalNumberToNumber[ONE] = 1;
digitalNumberToNumber[TWO] = 2;
digitalNumberToNumber[THREE] = 3;
digitalNumberToNumber[FOUR] = 4;
digitalNumberToNumber[FIVE] = 5;
digitalNumberToNumber[SIX] = 6;
digitalNumberToNumber[SEVEN] = 7;
digitalNumberToNumber[EIGHT] = 8;
digitalNumberToNumber[NINE] = 9;

function constructLinesFromFile(fileContent) {
  return fileContent.split('\n');
}

function constructCharactersByLine(lines) {
  return lines.map(splitLineIntoCharacterLengths);

  function splitLineIntoCharacterLengths(line) {
    let characterArray = [];

    for (i = 0; i < line.length; i += 3) {
      characterArray.push(line.slice(i , i + 3));
    }

    return characterArray;
  }
}

function constructAccountNumberDigits(lines) {
  let accountNumberDigits = [];

  while(lines.length) {
    accountNumberDigits.push(createDigitsFromLines(lines));
  }

  return accountNumberDigits;

  function createDigitsFromLines(lines) {
    let accountNumberLine = lines.splice(0, 4);

    return accountNumberLine[0].map((character, index) => {
      return character + 
             accountNumberLine[1][index] + 
             accountNumberLine[2][index]; 
    });
  }
}

function printAccountNumber(accountNumbers) {
  let convertedAccountNumbers = accountNumbers.map(convertDigitsToNumbers);

  function convertDigitsToNumbers(digits) {
    return digits.map((digit) => {
      return digitalNumberToNumber[digit];
    });
  }

  convertedAccountNumbers.forEach((accountNumber) => {
    console.log(accountNumber.join(''));
  });
}

function printAccountNumbersFromFile(
  filePath,
  constructLinesFromFile, 
  constructCharactersByLine,
  constructAccountNumberDigits,
  printAccountNumber
) {
  return fs.readFile(filePath, (err, data) => {
    if (err) throw err;

    let lines = constructLinesFromFile(data.toString());
    let linesWithCharacters = constructCharactersByLine(lines);
    let accountNumbers = constructAccountNumberDigits(linesWithCharacters);
    printAccountNumber(accountNumbers);
  });
}

printAccountNumbersFromFile(
  testfile, 
  constructLinesFromFile, 
  constructCharactersByLine,
  constructAccountNumberDigits,
  printAccountNumber
);