# Bank OCR Kata
Link to Kata: https://github.com/testdouble/contributing-tests/wiki/Bank-OCR-kata
Run the project against the test data using `node index.js`

## User Story 1 -- Read File Input and Print Account Numbers
* create test file and read it
* read lines across by 3 digits
* combine arrays to get whole digit strings
* lookup digit string values using map
* print account numbers from input

## User Story 2 -- Validate Account Numbers Through Checksum
* implement checksum calculation
* validate outputs against checksum

## User Story 3 -- Write File with Illegible or Illegal Labels
* if output is invalid according to checksum, append ` ERR`
* default to `?` in the case that a digit is not found in the number map
* if output contains a `?`, append ` ILL`
* change output stream from stdout to output file

## User Story 4 -- Attempt to Fix Erroneous Inputs
* read new file, filtering out valid lines
* attempt to add `|` or `_` for blank spaces to find a match in number map
* if only one match is found, rewrite line with new account number

## MISC
* Write unit tests for individual functions to validate output
* Break `index.js` into discrete parts with single responsibility
    * I/O module, file reading, writing
    * data manipulation for building account numbers from strings
    * checksum validation
    * error fixer 
* Evaluate whether `readline` is the best choice for this problem
* break current implementation into smaller parts